## 容器型笔记处理器

下列代码可以自动识别包含子卡片的“容器型笔记”，并自动修改样式，使容器型笔记更像储物盒（木质光泽）

### Query

```elixir
$IsPrototype(this)==false &
$IsTemplate(this)==false &
$Name(this)!="Prototypes" &
$Name(this)!="Templates" &
$Prototype!="Reference" &
$AgentQuery(this)=="" &
$ChildCount(this)>=1;
```

### Action

```elixir
$Color="#F5D29D";
$AccentColor="#A46507";
$Pattern="radial";
```