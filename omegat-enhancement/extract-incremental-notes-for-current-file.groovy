/* :name=生成当前文件的渐进笔记 :description=... 
 * #Purpose:  Write all source segments to a file
 * #Files:  Writes 'source_target.txt' in the 'script_output' subfolder
 *  in the current project's root
 * 
 * @author: Kos Ivantsov
 * @date: 2013-07-16
 * @version:  0.2
 */
 
/*
 * Here you can specify delimiters for source and target, and segments
 */
 
source_target_delim = '\n' //one line break
segment_delim = "\n"*2 //three line breaks

import static javax.swing.JOptionPane.*
import static org.omegat.util.Platform.*

// abort if a project is not opened yet
def prop = project.projectProperties
if (!prop) {
  final def title = '未打开学习项目'
  final def msg = '先打开一个学习项目，然后再试一次。'
  showMessageDialog null, msg, title, INFORMATION_MESSAGE
  return
}

def folder = prop.projectRoot + '/script_output'
def files = project.projectFiles
def infile = editor.currentFile

for (i in 0 ..< files.size()) {
  fi = files[i]
  if (fi.filePath != infile) { continue; }

  def outfileloc = folder + '/渐进笔记_' + fi.filePath
  outfile_handle = new File(outfileloc)
  if (! (new File(folder)).exists()) {
    (new File(folder)).mkdir()
  }
  outfile_handle.write("", 'UTF-8')
  def count = 0

  marker = "+${'='*(fi.filePath.size() * 2 + 2)}+\n"
  outfile_handle.append("$marker  $fi.filePath  \n$marker\n", 'UTF-8')
  for (j in 0 ..< fi.entries.size()) {
    ste = fi.entries[j]
    source = ste.getSrcText()
    //target = project.getTranslationInfo(ste) ? project.getTranslationInfo(ste).translation : null
    note = project.getTranslationInfo(ste) ? project.getTranslationInfo(ste).getNote() : null
    //if (target == null) { target = '' }
    //else if (target.size() == 0 ) { target = "<EMPTY>" }
    if (note != null && note.size() > 0) {
      outfile_handle.append source + source_target_delim + "\n> " + note + segment_delim, 'UTF-8'
      count++
    }
  }

  console.println count +" 条笔记碎片已存入 "+ outfile_handle
  final def title = '笔记碎片已保存'
  final def msg = count +" 条笔记碎片"+"\n"+"已存入\n"+ outfile_handle
  final def cmd = ['open', outfile_handle.toString()]
  // showMessageDialog null, msg, title, INFORMATION_MESSAGE
  console.println cmd.execute().text
}

return
